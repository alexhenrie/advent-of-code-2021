
use std::fs::File;
use std::io;
use std::io::BufRead;

fn main() {
    let file = File::open("input.txt").unwrap();
    let (mut x, mut y) = (0, 0);
    let mut aim = 0;
    for line in io::BufReader::new(file).lines() {
        let line = line.unwrap();
        let cells: Vec<&str> = line.split(' ').collect();
        let direction = cells[0];
        let magnitude = cells[1].parse::<i32>().unwrap();
        match direction {
            "forward" => {
                x += magnitude;
                y += magnitude * aim;
            },
            "up" => aim -= magnitude,
            "down" => aim += magnitude,
            _ => panic!(),
        }
    }
    println!("{}", x * y);
}
