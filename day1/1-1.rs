use std::fs::File;
use std::io;
use std::io::BufRead;

fn main() {
    let file = File::open("input.txt").unwrap();
    let mut count = 0;
    let mut last_depth = i32::MAX;
    for line in io::BufReader::new(file).lines() {
        let depth = line.unwrap().parse::<i32>().unwrap();
        if depth > last_depth {
            count += 1;
        }
        last_depth = depth;
    }
    println!("{}", count);
}
