use std::fs::File;
use std::io;
use std::io::BufRead;

fn main() {
    let file = File::open("input.txt").unwrap();
    let mut count = 0;
    let mut depths = [i32::MAX; 4];
    let mut i = 0;
    for line in io::BufReader::new(file).lines() {
        let mut prev_sum = 0;
        let mut this_sum = 0;
        depths[i] = line.unwrap().parse::<i32>().unwrap();
        i = (i + 1) % 4;
        if depths[i] == i32::MAX {
            continue;
        }
        for j in 0..4 {
            if j < 3 {
                prev_sum += depths[i]
            }
            if j > 0 {
                this_sum += depths[i]
            }
            i = (i + 1) % 4;
        }
        if this_sum > prev_sum {
            count += 1;
        }
    }
    println!("{}", count);
}
